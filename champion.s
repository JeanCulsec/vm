.name "Angerfist 2.0"
.comment "Jump Around"
.extend

mur:	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1

	live	%0
	sti	r1,%0,r2
	ld	0,r2
	fork	%:live
	
start:	live	%1
	sti	r1,%:live,%0x1
	zjmp	%:start

live:	live	%1
	sti	r1,%:live,%0x1
	ld	%46,r2
	zjmp	%:live

st:	live	%:st
	sti	r1,%:live,%1
	sti	r1,%:live,%10
	sti	r1,%:live,%2
	sti	r1,%:live,%20
	sti	r1,%:live,%3
	sti	r1,%:live,%30
	sti	r1,%:live,%4
	sti	r1,%:live,%40
	sti	r1,%:live,%5
	sti	r1,%:live,%50
	sti	r1,%:live,%6
	sti	r1,%:live,%60
	sti	r1,%:live,%7
	sti	r1,%:live,%71
	sti	r1,%:live,%72
	sti	r1,%:live,%73
	sti	r1,%:live,%74
	sti	r1,%:live,%75
	sti	r1,%:live,%76
	sti	r1,%:live,%77
	sti	r1,%:live,%78
	sti	r1,%:live,%79
	sti	r1,%:live,%41
	sti	r1,%:live,%12
	sti	r1,%:live,%89
	sti	r1,%:live,%95
	sti	r1,%:live,%54
	sti	r1,%:live,%21
	sti	r1,%:live,%95
	zjmp	%:st

end:	ld	20,r3
	and	r2,%:st,r3
	live	%20
	zjmp	%:end

mur2:	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
	zjmp %1
